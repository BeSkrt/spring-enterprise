package be.vdab.springEnterprise.beers.repositories;

import be.vdab.springEnterprise.MainApp;
import be.vdab.springEnterprise.beers.entities.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {MainApp.class})
class CategoryRepositoryTest {
    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void getCategoryById() {
        Optional<Category> category = categoryRepository.findById(1L);

        assertEquals("TestCategory", category.get().getName());
    }
}