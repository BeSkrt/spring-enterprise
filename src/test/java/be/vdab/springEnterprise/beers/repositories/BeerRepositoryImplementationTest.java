package be.vdab.springEnterprise
        .beers.repositories;

/**
 * @author Sven Wittoek
 * created on Monday, 18/10/2021
 */
//@SpringBootTest(classes = {BeerRepositoryImplementation.class, MainApp.class})
//public class BeerRepositoryImplementationTest {
//    @Autowired
//    private BeerRepositoryImplementation beerRepositoryImplementation;
//    @Autowired
//    private JdbcTemplate template;
//
//    @Test
//    public void getBeerByIdTest() {
//        Beer beer = beerRepositoryImplementation.getBeerById(1);
//        Assertions.assertEquals("TestBeer", beer);
//    }
//
//    @Test
//    public void setStockTest() {
//        beerRepositoryImplementation.setStock(1, 50);
//        Map<String, Object> el = template.queryForMap(
//                "select Stock from Beers where id = 1");
//        Assertions.assertEquals(el.get("Stock").toString(), "50");
//    }
//
//    @Test
//    @DirtiesContext
//    public void getBeerByAlcoholTest() {
//        List<Beer> el = beerRepositoryImplementation.getBeerByAlcohol(7);
//        String string = "{NAME=TestBeer, PRICE=2.75, STOCK=50, ALCOHOL=7.0}";
//        List<String> stringList = new ArrayList<>();
//        stringList.add(string);
//        Assertions.assertEquals(stringList, el);
//    }
//}
