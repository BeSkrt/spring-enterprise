package be.vdab.springEnterprise.beers.repositories;

import be.vdab.springEnterprise.MainApp;
import be.vdab.springEnterprise.beers.entities.Brewer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {MainApp.class})
class BrewerRepositoryTest {
    @Autowired
    private BrewerRepository brewerRepository;

    @Test
    void getBrewerById() {
        Optional<Brewer> brewer = brewerRepository.findById(1L);

        assertEquals("TestBrewer", brewer.toString());
    }
}