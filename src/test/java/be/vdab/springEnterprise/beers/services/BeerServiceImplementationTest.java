package be.vdab.springEnterprise.beers.services;

import be.vdab.springEnterprise.MainApp;
import be.vdab.springEnterprise.beers.beerOrders.orders.BeerOrder;
import be.vdab.springEnterprise.beers.repositories.BeerOrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {MainApp.class})
@Transactional
//@WithMockUser(roles = {"ADULT"})
class BeerServiceImplementationTest {
    @Autowired
    private BeerService beerService;
    @Autowired
    private BeerOrderRepository beerOrderRepository;

    @Test
    void orderBeer() {
        long beerId = beerService.orderBeer("TestBeer", 1, 2);
        BeerOrder beerOrder = beerOrderRepository.findById(beerId).orElse(null);
        assertEquals("TestBeer", beerOrder.getName());
        assertEquals(1, beerOrder.getItems().get(0).getBeer().getId());
        assertEquals(2, beerOrder.getItems().get(0).getNumber());
        assertEquals(1, beerOrder.getItems().size());
    }

    @Test
    void orderBeers() {
        long beerId = beerService.orderBeers("TestOrder", new long[][]{{1, 5}, {2, 10}});
        BeerOrder beerOrder = beerOrderRepository.findById(beerId).get();
        assertEquals("TestOrder", beerOrder.getName());
        assertEquals(1, beerOrder.getItems().get(0).getBeer().getId());
        assertEquals(5, beerOrder.getItems().get(0).getNumber());

        assertEquals(2, beerOrder.getItems().get(1).getBeer().getId());
        assertEquals(10, beerOrder.getItems().get(1).getNumber());

        assertEquals(2, beerOrder.getItems().size());
    }
}