package be.vdab.springEnterprise.beers.beerOrders.repositories;

import be.vdab.springEnterprise.MainApp;
import be.vdab.springEnterprise.beers.beerOrders.orders.BeerOrder;
import be.vdab.springEnterprise.beers.beerOrders.orders.BeerOrderItem;
import be.vdab.springEnterprise.beers.repositories.BeerOrderRepository;
import be.vdab.springEnterprise.beers.repositories.BeerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = {MainApp.class})
class BeerOrderRepositoryImplementationTest {
    private static final String TEST_BEER_ORDER = String.format("%d: %s%nItems: %s%n", 1, "TestOrder", "[TestBeer]");
    private static final long TEST_BEER_ORDER_ID = 1;

    @Autowired
    private BeerOrderRepository beerOrderRepository;
    @Autowired
    private BeerRepository beerRepository;

    @Test
    @Transactional
    void saveOrder() {
        BeerOrder order = new BeerOrder();
        order.setName("Testing this");
        List<BeerOrderItem> items = new ArrayList<>();
        items.add(new BeerOrderItem(beerRepository.getBeerById(1), 5));
        order.setItems(items);
        long id = beerOrderRepository.save(order).getId();
        Assertions.assertEquals(order, beerOrderRepository.findById(id).orElse(null));
    }

    @Test
    @Transactional
    void getBeerOrderById() {
        Assertions.assertEquals(TEST_BEER_ORDER,
                beerOrderRepository.findById(TEST_BEER_ORDER_ID).get().toString());
    }
}
