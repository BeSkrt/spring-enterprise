open module be.vdab.springEnterprise {
    requires spring.core;
    requires spring.context;
    requires spring.beans;

    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.boot.starter.jdbc;
    requires spring.jdbc;
    requires spring.boot.starter.data.jpa;
    requires spring.data.jpa;
    requires spring.data.commons;
    requires spring.tx;
    requires org.hibernate.orm.core;

    requires spring.boot.starter.security;
    requires spring.security.core;
    requires spring.security.config;

    requires spring.web;
    requires tomcat.embed.core;
    requires com.sun.xml.bind;
    requires jackson.annotations;

    requires java.annotation;
    requires java.validation;

    requires java.sql;
    requires java.persistence;

    requires java.xml.bind;
    requires net.bytebuddy;

}