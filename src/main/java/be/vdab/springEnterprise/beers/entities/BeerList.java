package be.vdab.springEnterprise.beers.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class BeerList {

    private List<Beer> beers;

    public BeerList() {
    }

    public BeerList(List<Beer> beers) {
        this.beers = beers;
    }

    @JsonProperty("list")
    @XmlElementWrapper(name = "beers")
    @XmlElement(name = "beer")
    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }
}
