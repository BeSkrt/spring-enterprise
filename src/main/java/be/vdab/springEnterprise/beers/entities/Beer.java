package be.vdab.springEnterprise.beers.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Beers")
@NamedQuery(name = "findByAlcohol", query = "select b from Beer b where b.alcohol = :alcohol")
@XmlRootElement
public class Beer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Price")
    private float price;

    @Column(name = "Stock")
    private int stock;

    @Column(name = "Alcohol")
    private float alcohol;

    @Version
    @Column(name = "Version")
    private int version;

    @ManyToOne
    @JoinColumn(name = "BrewerId")
    private Brewer brewer;

    @ManyToOne
    @JoinColumn(name = "CategoryId")
    private Category category;

    public Beer() {
    }

    public Beer(String name, float price, float alcohol) {
        this.name = name;
        this.price = price;
        this.alcohol = alcohol;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(float alcohol) {
        this.alcohol = alcohol;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Brewer getBrewer() {
        return brewer;
    }

    public void setBrewer(Brewer brewer) {
        this.brewer = brewer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beer beer = (Beer) o;
        return Double.compare(beer.getPrice(), getPrice()) == 0 && Double.compare(beer.getAlcohol(), getAlcohol()) == 0 && getStock() == beer.getStock() && Objects.equals(getName(), beer.getName()) && Objects.equals(getBrewer(), beer.getBrewer()) && Objects.equals(getCategory(), beer.getCategory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPrice(), getAlcohol(), getStock(), getBrewer(), getCategory());
    }


    @Override
    public String toString() {
        return "Beer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", alcohol=" + alcohol +
                ", version=" + version +
                ", brewer=" + brewer +
                ", category=" + category +
                '}';
    }
}
