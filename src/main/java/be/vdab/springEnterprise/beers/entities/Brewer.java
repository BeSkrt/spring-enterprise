package be.vdab.springEnterprise.beers.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Brewers")
@XmlRootElement
public class Brewer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Address")
    private String address;

    @Column(name = "City")
    private String city;

    @Column(name = "ZipCode")
    private String zipCode;

    @Column(name = "Turnover")
    private int turnover;

    @OneToMany(mappedBy = "brewer", cascade = CascadeType.REMOVE)
    private List<Beer> beers;

    public Brewer() {
    }

    public Brewer(String name, String address, String city, String zipCode, int turnover, List<Beer> beers) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.turnover = turnover;
        this.beers = beers;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public int getTurnover() {
        return turnover;
    }

    public void setTurnover(int turnover) {
        this.turnover = turnover;
    }

    @XmlTransient
    @JsonIgnore
    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brewer brewer = (Brewer) o;
        return getTurnover() == brewer.getTurnover() && Objects.equals(getName(), brewer.getName()) && Objects.equals(getAddress(), brewer.getAddress()) && Objects.equals(getZipCode(), brewer.getZipCode()) && Objects.equals(getCity(), brewer.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAddress(), getZipCode(), getCity(), getTurnover());
    }

    @Override
    public String toString() {
        return "Brewer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", turnover=" + turnover +
                '}';
    }

}
