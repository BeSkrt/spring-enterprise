package be.vdab.springEnterprise.beers.repositories;

import be.vdab.springEnterprise.beers.beerOrders.orders.BeerOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeerOrderRepository extends JpaRepository<BeerOrder, Long> {
}
