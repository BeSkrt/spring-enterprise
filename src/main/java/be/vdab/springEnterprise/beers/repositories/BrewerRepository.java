package be.vdab.springEnterprise.beers.repositories;

import be.vdab.springEnterprise.beers.entities.Brewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrewerRepository extends JpaRepository<Brewer, Long> {
}
