package be.vdab.springEnterprise.beers.repositories;

import be.vdab.springEnterprise.beers.entities.Beer;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Primary
public interface BeerRepository extends JpaRepository<Beer, Long> {

    default Beer getBeerById(long id) {
        return findById(id).orElse(null);
    }

    default void updateBeer(Beer beer) {
        save(beer);
    }

    @Query(name = "findByAlcohol")
    List<Beer> getBeerByAlcohol(float alcohol);

    List<Beer> findBeersByNameContainingOrderByNameDesc(String name);

    List<Beer> findBeersByStockLessThan(int number);

    @Modifying
    @Transactional
    @Query(value = "update Beer b set b.price = b.price * (?1 / 100.0 + 1)")
    void raisePriceOfBeers(float percentage);

    default List<Beer> findByExample(Beer probe) {
        List<String> pathsToIgnore = new ArrayList<>();
        pathsToIgnore.add("id");
        if (probe.getStock() == 0) {
            pathsToIgnore.add("stock");
        }
        if (probe.getPrice() == 0) {
            pathsToIgnore.add("price");
        }
        if (probe.getAlcohol() == 0) {
            pathsToIgnore.add("alcohol");
        }
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAll()
                .withIgnorePaths(pathsToIgnore.toArray(new String[0]))
                .withMatcher("name", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withIgnoreCase();
        Example<Beer> example = Example.of(probe, exampleMatcher);

        return findAll(example);
    }

}
