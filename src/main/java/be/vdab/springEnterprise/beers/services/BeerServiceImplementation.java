package be.vdab.springEnterprise.beers.services;

import be.vdab.springEnterprise.beers.beerOrders.dto.BeerOrderDto;
import be.vdab.springEnterprise.beers.beerOrders.orders.BeerOrder;
import be.vdab.springEnterprise.beers.beerOrders.orders.BeerOrderItem;
import be.vdab.springEnterprise.beers.entities.Beer;
import be.vdab.springEnterprise.beers.exceptions.InvalidBeerException;
import be.vdab.springEnterprise.beers.exceptions.InvalidNumberException;
import be.vdab.springEnterprise.beers.repositories.BeerOrderRepository;
import be.vdab.springEnterprise.beers.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BeerServiceImplementation implements BeerService {
    public static final int BEER_INDEX = 0;
    public static final int AMOUNT_INDEX = 1;
    private BeerRepository beerRepository;
    private BeerOrderRepository beerOrderRepository;

    @Autowired
    public void setBeerRepository(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Autowired
    public void setBeerOrderRepository(BeerOrderRepository beerOrderRepository) {
        this.beerOrderRepository = beerOrderRepository;
    }

    @Override
    @Transactional(rollbackFor = {InvalidBeerException.class, InvalidNumberException.class})
    public long orderBeer(String name, long beerId, int number) throws InvalidBeerException, InvalidNumberException {
        return orderBeers(name, new long[][]{{beerId, number}});
    }

    @Override
    @Transactional(rollbackFor = {InvalidBeerException.class, InvalidNumberException.class})
    public long orderBeers(String name, long[][] order) throws InvalidBeerException, InvalidNumberException {
        BeerOrder beerOrder = new BeerOrder();
        List<BeerOrderItem> beerOrderItemList = new ArrayList<>();

        for (long[] beerOrderArray : order) {
            BeerOrderItem beerOrderItem = new BeerOrderItem();
            beerOrderItem.setBeer(beerRepository.getBeerById(beerOrderArray[BEER_INDEX]));
            beerOrderItem.setNumber((int) beerOrderArray[AMOUNT_INDEX]);
            beerOrderItemList.add(beerOrderItem);
            Beer beer = beerRepository.getBeerById(beerOrderArray[BEER_INDEX]);
            if (beer == null) {
                throw new InvalidBeerException();
            }
            if (beerOrderArray[AMOUNT_INDEX] < 0) {
                throw new InvalidNumberException();
            }
            beer.setStock(beer.getStock() - beerOrderItem.getNumber());
        }

        beerOrder.setName(name);
        beerOrder.setItems(beerOrderItemList);
        return beerOrderRepository.save(beerOrder).getId();
    }

    @Override
    public long orderBeer(BeerOrderDto beerOrderDto) {
        return orderBeer(beerOrderDto.getName(), beerOrderDto.getId(), (int) beerOrderDto.getNumber());
    }
}
