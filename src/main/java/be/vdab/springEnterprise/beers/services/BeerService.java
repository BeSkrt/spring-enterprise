package be.vdab.springEnterprise.beers.services;

import be.vdab.springEnterprise.beers.beerOrders.dto.BeerOrderDto;
import be.vdab.springEnterprise.beers.exceptions.InvalidBeerException;
import be.vdab.springEnterprise.beers.exceptions.InvalidNumberException;
//import org.springframework.security.access.annotation.Secured;

//@Secured("ROLE_ADULT")
public interface BeerService {
    //    @Secured("ROLE_ADULT")
    long orderBeer(String name, long beerId, int number) throws InvalidBeerException, InvalidNumberException;

    long orderBeers(String name, long[][] order) throws InvalidBeerException, InvalidNumberException;

    long orderBeer(BeerOrderDto beerOrderDto);
}
