package be.vdab.springEnterprise.beers.beerOrders.orders;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "BeerOrders")
@XmlRootElement
public class BeerOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;

    @Column(name = "Name")
    private String name;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "BeerOrderId")
    private List<BeerOrderItem> items = new ArrayList<>();

    public BeerOrder() {
    }

    public BeerOrder(String name, List<BeerOrderItem> items) {
        this.name = name;
        this.items = items;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElementWrapper(name = "orderedItems")
    @XmlElement(name = "beerOrderItem")
    public List<BeerOrderItem> getItems() {
        return items;
    }

    public void setItems(List<BeerOrderItem> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeerOrder beerOrder = (BeerOrder) o;
        return Objects.equals(getName(), beerOrder.getName()) && Objects.equals(getItems(), beerOrder.getItems());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getItems());
    }

    @Override
    public String toString() {
        return String.format("%d: %s%nItems: %s%n",
                getId(),
                getName(),
                getItems()
        );
    }
}
