package be.vdab.springEnterprise.beers.beerOrders.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public class BeerOrderDto {
    @NotBlank
    private String name;
    @PositiveOrZero
    private long id;
    @Positive
    private int number;

    public BeerOrderDto() {
    }

    public BeerOrderDto(String name, long id, int number) {
        this.name = name;
        this.id = id;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
