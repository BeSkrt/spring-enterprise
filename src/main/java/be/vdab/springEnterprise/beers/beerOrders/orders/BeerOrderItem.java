package be.vdab.springEnterprise.beers.beerOrders.orders;

import be.vdab.springEnterprise.beers.entities.Beer;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@Entity
@Table(name = "BeerOrderItems")
@XmlRootElement
public class BeerOrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "BeerId")
    private Beer beer;

    @Column(name = "Number")
    private int number;

    public BeerOrderItem() {
    }

    public BeerOrderItem(Beer beer, int number) {
        setBeer(beer);
        setNumber(number);
    }

    public long getId() {
        return id;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeerOrderItem that = (BeerOrderItem) o;
        return getNumber() == that.getNumber() && Objects.equals(getBeer(), that.getBeer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBeer(), getNumber());
    }


    @Override
    public String toString() {
        return "BeerOrderItem{" +
                "id=" + id +
                ", beer=" + beer +
                ", number=" + number +
                '}';
    }

}
