package be.vdab.springEnterprise.beers.controllers;

import be.vdab.springEnterprise.beers.beerOrders.dto.BeerOrderDto;
import be.vdab.springEnterprise.beers.beerOrders.orders.BeerOrder;
import be.vdab.springEnterprise.beers.exceptions.InvalidBeerOrderException;
import be.vdab.springEnterprise.beers.repositories.BeerOrderRepository;
import be.vdab.springEnterprise.beers.services.BeerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value = {"/orders", "/Orders"})
public class OrdersRestController {

    private BeerService beerService;

    private BeerOrderRepository beerOrderRepository;

    public OrdersRestController(BeerService beerService, BeerOrderRepository beerOrderRepository) {
        this.beerService = beerService;
        this.beerOrderRepository = beerOrderRepository;
    }

    @GetMapping(value = "{id:^\\d+$}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<BeerOrder> getBeerOrder(@PathVariable long id) {
        BeerOrder beerOrder = beerOrderRepository.findById(id).orElse(null);
        if (beerOrder == null) {
            throw new InvalidBeerOrderException();
        } else {
            return new ResponseEntity<>(beerOrder, HttpStatus.OK);
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity orderBeer(@RequestBody @Valid BeerOrderDto beerOrderDto, HttpServletRequest servletRequest) {
        long beerId = beerService.orderBeer(beerOrderDto);
        URI location = URI.create(servletRequest.getRequestURL() + "/" + beerId);
        return ResponseEntity.created(location).build();
    }
}
