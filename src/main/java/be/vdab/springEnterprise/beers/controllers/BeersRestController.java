package be.vdab.springEnterprise.beers.controllers;

import be.vdab.springEnterprise.beers.entities.Beer;
import be.vdab.springEnterprise.beers.entities.BeerList;
import be.vdab.springEnterprise.beers.repositories.BeerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = {"/beers", "/Beers"})
public class BeersRestController {
    private BeerRepository beerRepository;

    public BeersRestController(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @GetMapping(value = "{id:^\\d+$}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Beer> getBeerById(@PathVariable long id) {
        Beer beer = beerRepository.getBeerById(id);
        if (beer == null) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(beer, HttpStatus.OK);
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<BeerList> getAllBeers(@RequestParam(required = false) String name,
                                                @RequestParam(required = false) Float alcohol) {
        List<Beer> beers = beerRepository.findAll();
        if (name != null) {
            String lowerCaseName = name.toLowerCase();
            beers = beers.stream().filter(b -> b.getName().contains(lowerCaseName))
                    .collect(Collectors.toList());
        }
        if (alcohol != null) {
            beers = beers.stream().filter(b -> b.getAlcohol() == alcohol).collect(Collectors.toList());
        }
        BeerList beerList = new BeerList(beers);

        return new ResponseEntity<>(beerList, HttpStatus.OK);
    }

//    @GetMapping(params = "alcohol", value = "{alcohol:^[-+]?[0-9]*\\.[0-9]+$}",
//            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
//    public ResponseEntity<BeerList> getBeerByAlcohol(@RequestParam float alcohol) {
//        List<Beer> beers = beerRepository.getBeerByAlcohol(alcohol);
//        BeerList beerList = new BeerList();
//        beerList.setBeers(beers);
//        if (!beers.isEmpty()) {
//            return ResponseEntity.ok().build();
//        } else {
//            throw new InvalidBeerException("Beers not found");
//        }
//    }
//
//    @GetMapping(params = "name", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
//    public ResponseEntity<BeerList> getBeerByName(@RequestParam String name) {
//        List<Beer> beers = beerRepository.findBeersByNameContainingOrderByNameDesc(name);
//        BeerList beerList = new BeerList();
//        beerList.setBeers(beers);
//        if (!beers.isEmpty()) {
//            return ResponseEntity.ok().build();
//        } else {
//            throw new InvalidBeerException("Beers not found");
//        }
//    }
}
