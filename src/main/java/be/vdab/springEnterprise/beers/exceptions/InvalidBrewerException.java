package be.vdab.springEnterprise.beers.exceptions;

public class InvalidBrewerException extends RuntimeException {
    public InvalidBrewerException() {
    }

    public InvalidBrewerException(String message) {
        super(message);
    }

    public InvalidBrewerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidBrewerException(Throwable cause) {
        super(cause);
    }

    public InvalidBrewerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
