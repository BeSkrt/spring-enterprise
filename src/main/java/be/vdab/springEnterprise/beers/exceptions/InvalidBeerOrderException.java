package be.vdab.springEnterprise.beers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid beer order")
public class InvalidBeerOrderException extends RuntimeException {

    public InvalidBeerOrderException() {
    }

    public InvalidBeerOrderException(String message) {
        super(message);
    }

    public InvalidBeerOrderException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidBeerOrderException(Throwable cause) {
        super(cause);
    }
}
