package be.vdab.springEnterprise.beers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid beer")
public class InvalidBeerException extends RuntimeException {

    public InvalidBeerException() {
    }

    public InvalidBeerException(String message) {
        super(message);
    }

    public InvalidBeerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidBeerException(Throwable cause) {
        super(cause);
    }

    public InvalidBeerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
