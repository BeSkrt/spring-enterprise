package be.vdab.springEnterprise.songs.repositories;

import be.vdab.springEnterprise.songs.entities.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SongRepository extends JpaRepository<Song, Long> {

    List<Song> findSongsByArtistOrderByTitleDesc(String artist);

    List<Song> findSongsByGenre(String genre);

    List<Song> findSongsByArtistAndGenre(String artist, String genre);

    List<Song> findSongsByYearBetween(int first, int last);

    List<Song> findByTitleLikeAllIgnoreCase(String title);

    default Song findSingleSong(long id) {
        return findById(id).orElse(null);
    }
}
