package be.vdab.springEnterprise.songs.controllers;

import be.vdab.springEnterprise.songs.entities.Song;
import be.vdab.springEnterprise.songs.repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"songs", "Songs"})
public class SongController {
    @Autowired
    private SongRepository songRepository;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<Song>> getAllSongs() {
        List<Song> songs = songRepository.findAll();
        return new ResponseEntity(songs, HttpStatus.OK);
    }

    @GetMapping(value = "{id}",
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Song> getSong(@PathVariable long id) {
        Song song = songRepository.findSingleSong(id);

        if(song != null) {
            return new ResponseEntity<>(song, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
