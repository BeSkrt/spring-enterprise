package be.vdab.springEnterprise;

import be.vdab.springEnterprise.beers.services.BeerService;
import be.vdab.springEnterprise.messages.entities.Message;
import be.vdab.springEnterprise.messages.repositories.MessageRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;

@SpringBootApplication
//@EnableGlobalMethodSecurity(securedEnabled = false)
public class MainApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(MainApp.class, args);

        MessageRepository messageRepository = ctx.getBean(MessageRepository.class);
        Message message = new Message(1, "Homer", "Hello!");
        Message message2 = new Message(2, "Behlül", "hallo!");
        messageRepository.createMessage(message);
        messageRepository.createMessage(message2);
    }
}
