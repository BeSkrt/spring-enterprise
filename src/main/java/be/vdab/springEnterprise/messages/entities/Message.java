package be.vdab.springEnterprise.messages.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.StringJoiner;

@XmlRootElement
@JsonPropertyOrder({"text", "author", "id"})
public class Message implements Serializable {

    private long id;
    @NotEmpty
    private String author;
    @NotEmpty
    private String text;

    private String displayMessage;

    public Message() {
    }

    public Message(long id, String author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
        displayMessage = author + ": " + text;
    }

    @XmlAttribute
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
        displayMessage = author + ": " + text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        displayMessage = author + ": " + text;
    }

    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }

    @XmlTransient
    @JsonIgnore
    public String getDisplayMessage() {
        return author + ": " + text;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Message.class.getSimpleName() + "[", "]")
                .add("id=" + id).add("author='" + author + "'")
                .add("text='" + text + "'").toString();
    }
}
