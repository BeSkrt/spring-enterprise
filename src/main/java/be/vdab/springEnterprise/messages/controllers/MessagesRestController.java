package be.vdab.springEnterprise.messages.controllers;

import be.vdab.springEnterprise.messages.entities.Message;
import be.vdab.springEnterprise.messages.entities.MessageList;
import be.vdab.springEnterprise.messages.exceptions.AuthorException;
import be.vdab.springEnterprise.messages.exceptions.IdException;
import be.vdab.springEnterprise.messages.exceptions.MessageNotFoundException;
import be.vdab.springEnterprise.messages.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value = {"/messages", "Messages"})
public class MessagesRestController {
    @Autowired
    private MessageRepository messageRepository;

    @GetMapping(value = "{id:^\\d+$}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Message> getMessage(@PathVariable("id") long id) {
        Message message = messageRepository.getMessageById(id);
        if (message == null) {
            throw new MessageNotFoundException();
        } else {
            return new ResponseEntity<>(message, HttpStatus.OK);
        }
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<MessageList> getAllMessages() {
        MessageList messages = messageRepository.getAllMessages();
        return new ResponseEntity(messages, HttpStatus.OK);
    }

    @GetMapping(params = {"author"}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<MessageList> getMessagesByAuthor(@RequestParam(value = "author") String authorName) {
        MessageList messages = messageRepository.getMessagesByAuthor(authorName);
        if (!messages.getMessages().isEmpty()) {
            return new ResponseEntity(messages, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity createMessage(@RequestBody @Valid Message message, HttpServletRequest request) {
        if (message.getId() == 0) {
            messageRepository.createMessage(message);
            URI location = URI.create(request.getRequestURL() + "/" + message.getId());
            return ResponseEntity.created(location).build();
        } else {
            throw new IdException("Id can't be configured");
        }
    }

    @PutMapping(path = "{id}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity updateMessage(@RequestBody @Valid Message message, @PathVariable long id) {
        if (id == message.getId() && id != 0) {
            messageRepository.updateMessage(message);
            return ResponseEntity.ok().build();
        } else {
            throw new IdException("Could not find message with id " + id);
        }
    }

    @PatchMapping(path = "{id}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity updateMessagePatch(@RequestBody Message patchMessage, @PathVariable long id) {
        if (id == patchMessage.getId() && id != 0) {
            Message message = messageRepository.getMessageById(id);
            if (patchMessage.getAuthor() != null) {
                message.setAuthor(patchMessage.getAuthor());
            } else {
                throw new AuthorException("Author not found");
            }
            if (patchMessage.getText() != null) {
                message.setText(patchMessage.getText());
            } else {
                throw new MessageNotFoundException("Text not found");
            }
            messageRepository.updateMessage(message);
            return ResponseEntity.ok().build();
        } else {
            throw new IdException(String.format("Could not find message with id %d", id));
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteMessage(@PathVariable long id) {
        messageRepository.deleteMessage(id);
        return ResponseEntity.ok().build();
    }


}
