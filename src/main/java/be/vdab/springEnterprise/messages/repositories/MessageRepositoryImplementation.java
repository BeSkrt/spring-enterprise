package be.vdab.springEnterprise.messages.repositories;

import be.vdab.springEnterprise.messages.entities.Message;
import be.vdab.springEnterprise.messages.entities.MessageList;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class MessageRepositoryImplementation implements MessageRepository {
    private Map<Long, Message> messages = new HashMap<>();

    @PostConstruct
    public void init() {
        Message message = new Message(1, "Homer", "Hello");
        messages.put(message.getId(), message);
    }

    @Override
    public synchronized Message getMessageById(long id) {
        return messages.get(id);
    }

    @Override
    public synchronized MessageList getAllMessages() {
        return new MessageList(new ArrayList<>(messages.values()));
    }

    @Override
    public synchronized MessageList getMessagesByAuthor(String author) {
        return new MessageList(messages.values().stream()
                .filter(m -> m.getAuthor().equals(author))
                .collect(Collectors.toList()));
    }

    @Override
    public synchronized Message createMessage(Message message) {
        if(message.getId() == 0) {
            message.setId(createId());
        }
        messages.put(message.getId(), message);
        return message;
    }

    @Override
    public synchronized Message updateMessage(Message message) {
        messages.put(message.getId(), message);
        return message;
    }

    @Override
    public synchronized void deleteMessage(long id) {
        messages.remove(id);
    }

    private long createId() {
        OptionalLong max = messages.keySet().stream().mapToLong(Long::longValue).max();
        return max.isEmpty()? 1 : max.getAsLong() + 1;
    }
}
