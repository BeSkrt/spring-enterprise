package be.vdab.springEnterprise.messages.repositories;

import be.vdab.springEnterprise.messages.entities.Message;
import be.vdab.springEnterprise.messages.entities.MessageList;

import java.util.List;

public interface MessageRepository {

    Message getMessageById(long id);
    MessageList getAllMessages();
    MessageList getMessagesByAuthor(String author);
    Message createMessage(Message message);
    Message updateMessage(Message message);
    void deleteMessage(long id);

}
